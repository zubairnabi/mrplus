#!/bin/sh
hadoop_dir=/state/partition1/hadoop-data
rm -rf ${hadoop_dir}
mkdir ${hadoop_dir}
mkdir ${hadoop_dir}/base-tmp
mkdir ${hadoop_dir}/dfs-name
mkdir ${hadoop_dir}/dfs-data
unset hadoop_dir