from ma.utils.clustersetup import CLUSTER_IPS

import getopt
import os
import sys

# the base path where the required files will be
remote_base_path = '~/'
local_base_path = ''

remote_nt_logger_path = remote_base_path + 'ma_logger.log'
remote_master_logger_path = remote_base_path + 'ma_master.log'
remote_nohup_path = remote_base_path + 'nohup.out'

nt_logger_path = local_base_path + 'ma_logger.log'
master_logger_path = local_base_path + 'ma_master.log'
nohup_path = local_base_path + 'nohup.out'

cmd = 'cat %s | egrep \'WARNING|ERROR\''
nohup_cmd = 'cat %s | egrep \'Error|Traceback|Exception\''

print('usage: python checkerrors.py [-r check for errors/warnings on all remote machines]')

optlist, args = getopt.getopt(sys.argv[1:], 'r')
start_remote = False
for opt in optlist:
    if opt[0] == '-r':
        start_remote = True

if start_remote:
    for ip in CLUSTER_IPS:
        print("Errors/Warnings at " + ip)
        os.system(('ssh ' + ip + ' ' + cmd) % remote_nt_logger_path)
        os.system(('ssh ' + ip + ' ' + cmd) % remote_master_logger_path)
        os.system(('ssh ' + ip + ' ' + nohup_cmd) % remote_nohup_path)

print("Errors/Warnings on local")
os.system(cmd % nt_logger_path)
os.system(cmd % master_logger_path)
os.system(nohup_cmd % nohup_path)
