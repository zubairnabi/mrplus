import ma.const

import os
import sys


hadoop_exec = ma.const.XmlData.get_filepath_str_data(ma.const.xml_hadoop_exec)
dfs_jobs_dir = ma.const.XmlData.get_dfs_filepath_str_data(ma.const.xml_dfs_job_base_path) + ma.const.dfs_dir_sep

if len(sys.argv) != 4:
    print('usage: python cpfileshdfs.py [job_id] [first_file] [last_file]')
    sys.exit(-1)

# get argument variables    
job_id = int(sys.argv[1])
first_file = int(sys.argv[2])
last_file = int(sys.argv[3])+1

print("Copying input files")

for file_id in range(first_file, last_file):
    filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_map_input_filename, job_id, file_id)
    dfs_dest_path = ma.const.JobsXmlData.get_dfs_filepath_str_data(ma.const.xml_dfs_path_job_input, job_id)
    os.system(hadoop_exec + ' fs -copyFromLocal ' + filename + ' ' + dfs_dest_path)
    print("Copied", filename)
    
print("Done - Khuda-Hafiz!")

