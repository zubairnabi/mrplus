from ma.utils.clustersetup import CLUSTER_IPS

import getopt
import os
import sys


python_exec_path = ma.const.XmlData.get_filepath_str_data(ma.const.xml_python_exec)
# remove the flags from the path
python_exec_path = python_exec_path.split(' ')[0]
python_exec_path = os.path.basename(python_exec_path)
cmd = 'killall -9 ' + python_exec_path

print('usage: python endscript.py [-r to kill on all remote machines]')
print('Running command: %s' % cmd)

optlist, args = getopt.getopt(sys.argv[1:], 'r')
start_remote = False
for opt in optlist:
    if opt[0] == '-r':
        start_remote = True

if start_remote:
    for ip in CLUSTER_IPS:
        os.system('ssh ' + ip + ' ' + cmd)
        print("Killed python tasks at " + ip)

os.system(cmd)
print("Killed python on local")