from ma.utils.clustersetup import *
 
import os
import os.path
import sys


dest_dir = 'C:\\Users\\Ahmad\\Documents\\MR Improv\\Results\\'

if len(sys.argv) != 2:
    print('Usage: python transferstats_cluster.py [dest_dir]')
    sys.exit(-1)

dest_dir = dest_dir + sys.argv[1]
print("Destination Dir is:", dest_dir)
src_dir = STATS_DEST_PATH
print("Source Dir is:", src_dir)

# ensure the destination directory exists
if not os.path.exists(dest_dir):
    os.makedirs(dest_dir)

for i in [str(idx) for idx in range(len(CLUSTER_IPS))]:
    for file in COPY_STATS_FILES:
        filename = file % i
        cmd = "pscp -pw " + FRONT_END_PWD + " " + FRONT_END_USER + "@" + FRONT_END_IP + ":" + src_dir + filename + " \"" + dest_dir + "\""
        os.system(cmd)
        print("Transferred " + filename + " from " + i)

for file in COPY_STATS_FILES_MASTER:
    cmd = "pscp -pw " + FRONT_END_PWD + " " + FRONT_END_USER + "@" + FRONT_END_IP + ":" + src_dir + file + " \"" + dest_dir + "\""
    os.system(cmd)
    print("Transferred " + file + " from master")
    
print("Done - Khuda-Hafiz!")