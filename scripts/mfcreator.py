import ma.fs.dfs.dfsflags as dfsflags
import ma.const
import sys

if len(sys.argv) != 5:
    print('usage: python mfcreator.py [job_id] [starting_file_no] [no_input_files] [files_per_map]')
    sys.exit(-1)

job_id = int(sys.argv[1]) 
starting_file_no = int(sys.argv[2])
no_of_files = int(sys.argv[3])
files_per_map = int(sys.argv[4])


if __name__ == '__main__':
    
    xml_str = dfsflags.DfsFlags._DfsFlags__mapflag_template
    mfs = ""
    
    filename = str(job_id) + 'M%d.in'
    # create stub for each file
    for file_no in range(no_of_files / files_per_map):
        inputs = []
        for inp_file_no in range((file_no*files_per_map),(file_no*files_per_map)+files_per_map):
            no = starting_file_no + inp_file_no
            inputs.append((filename % no, 0, 0))
        mfs += dfsflags.DfsFlags.create_map_flags_stub(file_no, inputs)
    
    if no_of_files % files_per_map != 0:
        inputs = []
        for inp_file_no in range((no_of_files / files_per_map) * files_per_map, no_of_files):
            no = starting_file_no + inp_file_no
            inputs.append((filename % no, 0, 0))
        mfs += dfsflags.DfsFlags.create_map_flags_stub(file_no, inputs)
        
    xml_str = xml_str % mfs
    
    # write to file
    mf_filename = ma.const.XmlData.get_str_data(ma.const.xml_map_flags_filename)
    mf_fd = open(mf_filename, 'w+')
    mf_fd.write(xml_str)
    mf_fd.close()
    
    