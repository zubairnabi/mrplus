from ma.utils.clustersetup import *

import os
import os.path
import sys


if len(sys.argv) != 2:
    print('Usage: python transferstats.py [job_id]')
    sys.exit(-1)
    
job_id = int(sys.argv[1])

# ensure the destination directory exists
if not os.path.exists(STATS_DEST_PATH):
    os.makedirs(STATS_DEST_PATH)

for ip,i in [(CLUSTER_IPS[idx],str(idx)) for idx in range(len(CLUSTER_IPS))]:
    for file in COPY_STATS_FILES:
        filename = file % i
        src_filename = file % ''
        os.system('scp ' + BACK_END_ACCESS_USER + '@' + ip + ':' + REMOTE_RUN_PATH + src_filename + ' ' + STATS_DEST_PATH + filename)
    print('--- Transferred from', ip)
    
for file in COPY_STATS_FILES_MASTER:
    os.system('cp ' + os.path.join((MASTER_RUN_PATH % str(job_id)), file) + ' ' + STATS_DEST_PATH)

print("Done - Khuda-Hafiz!")
