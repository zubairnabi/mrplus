"""Note this reduce task runner is specifically made to handle heavy keys where
The data might be very large at a particular reduce worker. It doesn't write
The output to a dictionary rather produces to file where each key is on a
single line.
Conditions:
1. The key should not contain ':' in it anywhere
2. Any value in the output should not contain any newline character or a comma
3. The number of keys should be less, because each key makes up an input file

NOTE: It specifically works with codes.reducethresh.Reduce
"""

from ma.commons.core.reducetaskrunner import ReduceTaskRunner
from ma.commons.core.abstractreducer import AbstractReducer
from ma.commons.core.processrunner import ProcessRunner
from .partitioner import Partitioner
import ma.commons.core.constants as constants
import ma.const
import ma.log
import ma.utils.listutils as listutils
from ma.commons.core.maptaskrunner import MapTaskRunner

import sys
import pickle
import time
import os
import traceback



class MRReduceTaskRunnerHeavyKey(ReduceTaskRunner):
    
    def __init__(self, tip_info, user_module_name, user_class_name):
        
        ReduceTaskRunner.__init__(self, tip_info, user_module_name, user_class_name)
        
        self.__log = ma.log.get_logger("ma.codes")
        
    
    def startTask(self):
        """This func is called to start the user defined code as a separate 
        process
        """
        partitioner = Partitioner(self.tip_info.job_id)
        hash_value = partitioner.taskIdToHash(self.tip_info.task_id)
        
        input_dest_dir = ma.const.JobsXmlData.get_filepath_str_data(ma.const.xml_local_input_temp_dir, self.tip_info.job_id) + os.sep
        
        temp_filename_prefix = str(self.tip_info.job_id) + str(constants.REDUCE) + str(self.tip_info.task_id) + '_' 
        compute_temp_path = ma.const.JobsXmlData.get_filepath_str_data(ma.const.xml_local_compute_temp_dir, self.tip_info.job_id) + os.sep + temp_filename_prefix
        
        list_of_keys = []
        
        # to note the keys that are thresholded
        list_of_thresholded_keys = []
        
        for input_data in self.tip_info.input_data:
            if input_data[1] == constants.MAP:
                filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_map_output_filename_with_key, input_data[0], input_data[2], hash_value)
            else:
                self.__log.error("Error Reduce task %d has been given a reduce input: %s", self.tip_info.task_id)
            
            # filepath for the shuffled input
            filepath = input_dest_dir + filename
            
            try:
                self.__log.info('Input file to reduce %d: %s', self.tip_info.task_id, filepath)
                
                # open file and make files for all inputs
                f = open(filepath, 'r')
                list_of_keys_input = self.writeKeysOutputFiles(f, compute_temp_path)
                f.close()
                
                self.__log.info('Input file to reduce %d read completely: %s', self.tip_info.task_id, filepath)
                
                # add all keys to our key list
                list_of_keys = listutils.list_union(list_of_keys, list_of_keys_input)
                
            except IOError as err_msg:
                self.__log.error("Error while reading reduce input or running task: %s", err_msg)
                os._exit(-1)
        
        # get the thresholding value
        threshold_value = ma.const.JobsXmlData.get_float_data(ma.const.xml_threshold_value, self.tip_info.job_id)
        input_compute_buffer = ma.const.JobsXmlData.get_int_data(ma.const.xml_reduce_inp_compute_size, self.tip_info.job_id)
        
        # iterate through all keys
        for key in list_of_keys:
            list = []
            
            # filepath containing data for this specific key
            key_file = compute_temp_path + key
                        
            user_vars = [None, None, threshold_value, input_compute_buffer]
            
            #self.__log.info('Input to reduce %d function for key: %s', self.tip_info.task_id, key)
            
            th = eval( 'self.user_class.' + self.user_class_name + '(key, key_file, self.key_value_dict, user_vars)' )
            self.threads.append(th)
            
            # check if the Reducer is thresholdable
            thresholdable = th.thresholdable
            
            th.start()
            th.join()
            
            # remove the computation file
            os.remove(key_file)
            
            # if there is a valid threshold value and the Reducer is Thresholdable
            if threshold_value != None and thresholdable == AbstractReducer.THRESHOLDABLE:
                # if the key has been added to the dictionary by the reduce worker
                if key in self.key_value_dict:
                    final_key_value = self.key_value_dict[key][0]
                    
                    # if the key's value has crossed the threshold
                    if final_key_value > threshold_value:
                        print(("------ Reduce ID %d Job ID %d -----> KEY THRESHOLDED - %s" % (self.tip_info.task_id, self.tip_info.job_id, str(key))))
                        list_of_thresholded_keys.append((key, time.time(), self.tip_info.task_id))
            
        self.__log.info('Actual processing on reduce %d ENDED', self.tip_info.task_id)
        
        # write the dictionary to the output file
        try:
            #working out a reduce output file path name
            output_dest_dir = ma.const.JobsXmlData.get_filepath_str_data(ma.const.xml_local_output_temp_dir, 
                                                                        self.tip_info.job_id)
            
            output_filepath = output_dest_dir + os.sep + \
                    ma.const.JobsXmlData.get_str_data(ma.const.xml_reduce_output_filename, self.tip_info.job_id, self.tip_info.task_id)
                
            self.__log.info('Starting to write reduce %d output: %s', self.tip_info.task_id, output_filepath)
            fd = open(output_filepath, "w+")
            
            MapTaskRunner.write_keylines(fd, self.key_value_dict)
            
            fd.close()
            self.__log.info('Written reduce %d output: %s', self.tip_info.task_id, output_filepath)
            
            return [ True, list_of_thresholded_keys ]
        except IOError as err_msg:
            self.__log.error('Error while writing reduce output task-id %d, job-id %d: %s', self.tip_info.task_id, self.tip_info.job_id, err_msg)
            return [ False ]
                
            
if __name__ == "__main__":
    """This function is called by the process creator to transfer information
    about the reduce task to run
    """
    
    try:
        print("Running MRReduceTaskRunnerHeavyKey process pid", os.getpid())
        
        if len(sys.argv) == 5:
            user_module_name = sys.argv[1]
            user_class_name = sys.argv[2]
            job_id = sys.argv[3]
            task_id = sys.argv[4]
            #print user_module_name, user_class_name
            
            # get the filename which holds the reduce compute misc. output
            proc_temp_filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_reduce_proc_temp_filename, job_id, task_id)
            
            # get the pickled list data
            list = ProcessRunner.pull_pickled_data(proc_temp_filename, job_id)
            reducetipinfo = list.__getitem__(0)
            #print 'Job ID:', maptipinfo.job_id
            #print 'Task ID:', maptipinfo.task_id
            #print 'Map or reduce:', maptipinfo.map_or_reduce
            #print 'Struct data:', maptipinfo.struct_data
            #print 'Input data:', maptipinfo.input_data
            
            task_runner = MRReduceTaskRunnerHeavyKey(reducetipinfo, user_module_name, user_class_name)
            # open files, process, write output
            ret = task_runner.startTask()
            
            if not ret[0]:
                # some error occurred
                os._exit(-1)
            else:
                # pack and pickle the lists back to the ReduceTIP
                list_of_thresholded_keys = ret[1]
                
                list = [list_of_thresholded_keys]
                proc_temp_filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_reduce_proc_temp_filename, reducetipinfo.job_id, reducetipinfo.task_id)
                ProcessRunner.store_pickled_data(list, proc_temp_filename, reducetipinfo.job_id)
                
                # ran fine
                os._exit(0)
        else:
            print('Wrong number of arguments for ReduceTaskRunner process')
            os._exit(-1)
            
    except Exception as err_msg:
        traceback.print_exception()
        print("MRReduceTaskRunnerHeavyKey:__main__ Exception in running reduce: %s" % err_msg)
        os._exit(-1)
