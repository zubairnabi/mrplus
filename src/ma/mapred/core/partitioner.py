import ma.const

class Partitioner(object):
    
    """
    this class will help partition map intermediate output data across the different 
    reduce workers
    """
    
    def __init__(self, job_id):    
        """for a hash partitioner 
        1. it will look up the number of reduces from job xml
        2. 
        """
        self.no_of_reduces = ma.const.JobsXmlData.get_int_data(ma.const.xml_no_reduces, job_id)
        
        
    def partition(self, key):
        """ this function will return a hash value for the key mod R(number of reduces)
        """
        return hash(key) % self.no_of_reduces
    
    
    def taskIdToHash(self, task_id):
        """ this function converts a given reduce task ID to the hash value. 
        It helps in identifying the map output which needs to be shuffled to 
        each requesting reduce task
        """
        return task_id
    
    
    def returnAllHashVals(self):
        """ this function returns a list of all the hash values possible for
        the current job. This helps in creating all the output files which
        would need to be shuffled to every reduce
        """
        return list(range(self.no_of_reduces))
    