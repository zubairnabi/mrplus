"""@summary: Distributed File-system package

This package currently contains modules to interact with the Hadoop File
System (HDFS). The interaction takes place by implementing a SWIG interface
to the libhdfs provided in Lucene Hadoop (libhdfs provides similar 
functionality to GFS, but skips on some key features like atomic rename, etc.)

TODO: Complete Doc

@status: Under active development
@version: 0.0.0.1

@author: Ahmad Humayun, Saptarshi

"""

__all__ = ["_hdfs", "_hdfsfile"]

