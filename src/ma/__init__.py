"""@summary: Main package, containing MapReduce / improvements and core libs

mapred - package mimics the MapReduce / Hadoop implementation
mrimprov - package which improves on MapReduce's original framework.
    The work is a brain-child of Ahmad Humayun, Momina Azam and Dr. Umar Saif

@status: Under active development
@version: 0.0.0.1

@keyword Map: Part of MapReduce
@keyword Reduce: Part of MapReduce
@keyword Distubted Computing: Project Domain
@keyword Master-less Control: 
    
@organization: Lahore University of Management Sciences, Pakistan

@author: Ahmad Humayun, Momina Azam
@contact: <ahmad.humyn@gmail.com>, <momina.azam@gmail.com>

"""

__all__ = ["codes", "commons", "fs", "net", "mapred", "mr+master", "mrimprov", "stats", "utils"]


import os
import sys
import logging
import logging.config
from . import const

if __name__ == '__main__':
    test = 1
    #place to write tests for the complete module

def initializeLoggers():
    # TODO: Need to write sequence which writes down the right filepath
    # of the logging file
    i = 0
    