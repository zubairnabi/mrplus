"""Current this module has one function which can be used to init a logger

"""

import ma.const
import logging
import logging.config

def get_logger(logger_str):
    #get logging file path
    logging_fp = ma.const.ma_logging_filepath()
        
    #initialize logger for HDFS
    logging.config.fileConfig(logging_fp)
    return logging.getLogger(logger_str)