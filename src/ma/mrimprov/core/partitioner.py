class Partitioner(object):
    
    """
    this class will help partition map intermediate output data across the different 
    reduce workers
    """
    
    def __init__(self):
        
        """for a hash partitioner 
        1. it will look up the number of reduces from job xml
        for a partitioner dividing input sizewise
        2. it will look up the minimum size of the chunk
        """
        
        #TODO: get it from job xml
        self.min_size_of_chunk = None 
        
        
    def partition(self, key):
        
        """ this function will return a hash value for the key mod R(number of reduces)
        """
        
        #probably set according to size of partition
        pass
        