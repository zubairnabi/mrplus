from ma.commons.core.abstractmapper import AbstractMapper
import re


class Map(AbstractMapper):
    """this is the user defined map function 
    """ 
    
    def  __init__(self, filename, filecontents, key_value_dict):    
        """
        """
        
        AbstractMapper.__init__(self, filename, filecontents, key_value_dict)

        self.list_of_keys = ['holmes', 'sherlock']
    
    
    def map(self, filename, filecontents):
        """the user map function
        """
        
        # bisect word through alphanumeric chars
        #list_of_words = re.findall(r'\w+',filecontents)
        
        # bisect words on whitespace characters
        list_of_words = re.findall(r'\S+',filecontents)
        
        total_words = len(list_of_words)
        sum = 0
        for word in list_of_words:    
            if word.lower() in self.list_of_keys:
                self.emit(word.lower(), [1])
        
    