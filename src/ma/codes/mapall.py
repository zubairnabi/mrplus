from ma.commons.core.abstractmapper import AbstractMapper
import re


class Map(AbstractMapper):
    """this is the user defined map function 
    """
    
    def  __init__(self, filename, filecontents, key_value_dict):    
        """
        """
        
        AbstractMapper.__init__(self, filename, filecontents, key_value_dict)    
    
    
    def map(self, filename, filecontents):
        """the user map function
        """        
        
        # bisect word through alphanumeric chars
        #list_of_words = re.findall(r'\w+',filecontents)
        
        # bisect words on whitespace characters
        list_of_words = re.findall(r'\S+',filecontents)
        
        for word in list_of_words:
            self.emit(word,[1])

