from ma.commons.core.abstractmapper import AbstractMapper
import re


class Map(AbstractMapper):
    """this is the user defined map function 
    """
    
    def  __init__(self, filename, filecontents, key_value_dict):    
        """
        """
        
        AbstractMapper.__init__(self, filename, filecontents, key_value_dict)
        
        self.list_of_keys = ['ma.mf.ah', 'pd.pc.pa', 'qd.qc.qa', 'rd.rc.ra', 'ud.uc.ua', 'vd.vc.va', 'wd.wc.wa', 'td.tc.ta', 'sd.sc.sa', 'garbage.com']
        
    
    def map(self, filename, filecontents):
        """the user map function
        """
        
        # bisect word through alphanumeric chars
        #list_of_words = re.findall(r'\w+',filecontents)
        
        # bisect words on whitespace characters
        list_of_words = re.findall(r'\S+',filecontents)
        
        for word in list_of_words:    
            if word in self.list_of_keys:
                self.emit(word,[1])
                
