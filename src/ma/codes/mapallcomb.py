from ma.commons.core.abstractmapper import AbstractMapper
import re


class Map(AbstractMapper):
    """this map function is a map all function with a combiner at the end
    """
    
    def  __init__(self, filename, filecontents, key_value_dict):    
        """
        """
        
        AbstractMapper.__init__(self, filename, filecontents, key_value_dict, combinable=AbstractMapper.WITH_COMBINER)    
    
    
    def map(self, filename, filecontents):        
        """the user map function
        """        
        
        # bisect word through alphanumeric chars
        #list_of_words = re.findall(r'\w+',filecontents)
        
        # bisect words on whitespace characters
        list_of_words = re.findall(r'\S+',filecontents)
        
        for word in list_of_words:
            self.emit(word, [1])


    def combine(self, key, list_of_values):
        """this is the user defined combiner function run at the end of the
        map function
        """
        
        self.frequency_count = 0
        
        # iterate through list of values and add them
        for i in list_of_values:
            self.frequency_count += i
            
        return [self.frequency_count]
