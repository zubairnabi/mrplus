from ma.commons.core.abstractmapper import AbstractMapper
import re


class Map(AbstractMapper):
    """this is the user defined map function 
    """
    
    def  __init__(self, filename, filecontents, key_value_dict):    
        """
        """
        
        AbstractMapper.__init__(self, filename, filecontents, key_value_dict)
        
        self.list_of_keys = ['e526.d.akamaiedge.net']
    
    
    def map(self, filename, filecontents):
        """the user map function
        """
        
        akamai_link = self.list_of_keys[0]
        # bisect word through alphanumeric chars
        #list_of_words = re.findall(r'\w+',filecontents)
        
        # bisect words on whitespace characters
        list_of_words = re.findall(r'\S+',filecontents)
        
        total_words = len(list_of_words)
        sum = 0
        for word in list_of_words:    
            if word == akamai_link:
                sum += 1
        
        avg = (float(sum) / total_words) * 100.0         
        self.emit(akamai_link, [(avg, 1)])
        
