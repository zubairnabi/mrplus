import ma.const

import random
import os

no_of_files = 1
file_size = 32 * 1024 * 1024
words_inserted = 0
job_id = 9

main_dest_dir = "/state/partition1/datasets/"

keys = [ \
        ('suraj.lums.edu.pk/~ahmadh',5), \
        ('suraj.lums.edu.pk/~mominaa',5), \
        ('web.lums.edu.pk/~umar',5), \
        ('dns.akamai.com',1), \
        ('www.cnn.com',1), \
        ('sd.sc.sa',10), \
        ('pd.pc.pa',10), \
        ('qd.qc.qa',10), \
        ('rd.rc.ra',10), \
        ('ud.uc.ua',10), \
        ('vd.vc.va',10), \
        ('wd.wc.wa',10), \
        ('td.tc.ta',10), \
        ('sd.sc.sa',10), \
        ('pd.pc.pa',10), \
        ('qd.qc.qa',10), \
        ('rd.rc.ra',10), \
        ('ud.uc.ua',10), \
        ('vd.vc.va',10), \
        ('wd.wc.wa',10), \
        ('garbage.com/garbage',20) \
    ]



def make_list():
    return_list = []
    for i in keys:
        for y in range(i[1]):
            return_list.append(i[0])
    return return_list


if __name__ == '__main__':
    list = make_list()
    dir = main_dest_dir + str(job_id)
    
    # ensure the destination directory exists
    if not os.path.exists(dir):
        os.makedirs(dir)
    
    for file_no in range(no_of_files):    
        filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_map_input_filename, job_id, file_no)
        filepath = dir + os.sep + filename
        
        print(filepath)
        fd = open(filepath, 'w+')
        random.seed()
        
        curr_file_size = 0
        words_inserted = 0
        separator = ' '
        while curr_file_size < file_size:
            word = random.choice(list)
            word = word + separator
            curr_file_size += len(word)
            words_inserted += 1
            if words_inserted % 100 == 0:
                random.seed()
            fd.write(word)
        
        print('File output', filename, 'Word count', words_inserted)
        
        fd.close()
        