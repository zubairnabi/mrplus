import ma.const

import os
import operator
import os.path
import re
import sys

ips = ['192.168.100.1', '192.168.100.248', '192.168.100.249', '192.168.100.250', '192.168.100.251', '192.168.100.252', '192.168.100.253']
filepath = 'reduce_final.out'
out_filepath = 'struct_result.out'

if len(sys.argv) != 2:
    print('Usage: python gutenberg_results.py [job_id]')
    sys.exit(-1)
    
job_id = sys.argv[1]

output_dir = ma.const.JobsXmlData.get_str_data(ma.const.xml_local_output_temp_dir, job_id)

fd = open(filepath, 'r')
reduce_out_str = fd.read()
fd.close()

per_struct_count = []
struct_strings = {}

# iterate through every struct string
# while there is still more structs to process
while reduce_out_str != '':
    # adjust the string
    temp_str_idx = reduce_out_str.find('\n')
    temp_str = reduce_out_str[:temp_str_idx]
    reduce_out_str = reduce_out_str[temp_str_idx+1:]
    
    out_str = temp_str
    
    # regular expression matching
    match_list = re.findall(r'Struct\s+(\d+),\s+Reduce\s+id\s+(\d+),\s+Level\s+(\d+)\s+\|\s+(\d+)', temp_str)
    
    reduce_filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_reduce_output_filename, job_id, match_list[0][1])
    struct_id = match_list[0][0]
    
    print('Get file out', reduce_filename)
    
    reduce_filepath = os.path.join(output_dir, reduce_filename)
    
    # attempt to pull the file
    for ip in ips:
        ret = os.system('scp root@%s:%s .' % (ip, reduce_filepath))
        if ret == 0:
            break
        
    fd2 = open(reduce_filename, 'r')
    out_file_str = fd2.read()
    fd2.close()
    
    # match list from
    match_list = re.findall(r'\'(\w+)\':\s+\[(\d+)\]', out_file_str)
    
    # count the total occurence of all words in the struct
    total = 0
    out_str2 = ''
    for tpl in match_list:
        out_str2 += ' | ' + tpl[0] + ':' + tpl[1]
        total += int(tpl[1])
    
    out_str += '\t|| Total:' + str(total) + '\t'
    out_str += out_str2 + '\n'
    
    # put the string in the structs strings dict
    struct_strings[struct_id] = out_str
    per_struct_count.append((struct_id, total))
    
    os.remove(reduce_filename)

per_struct_count.sort(key=operator.itemgetter(1), reverse=True)

fout = open(out_filepath, 'w+')

for struct in per_struct_count:
    write_str = struct_strings[struct[0]]
    fout.write(write_str)
    
fout.close()
