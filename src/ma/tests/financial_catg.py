import re
import os
import os.path
from urllib.request import urlopen
from urllib.request import urlretrieve

job_id = 10
no_financial_depts = 160
files_per_dept = 10
file_size = 20 * 1024 * 1024
main_copy_dir = "/state/partition1/datasets/"

__start = ord('A')
__limit = ord('Z') - ord('A') + 1

def get_next_id(current_id):
    """Considering each file transfer has a string ID, this function 
    fetches the next ID in line. It moves so: A -> B, Z -> AA,
    AZ -> BA
    """
    
    idx = -1
    length = len(current_id)
    spillover = 1
    while spillover > 0 and abs(idx) <= length:
        temp = ord(current_id[idx]) - __start
        x = chr(((temp + spillover) % __limit) + __start)
        spillover = (temp + spillover) / __limit
        if idx < -1:
            tail = current_id[idx + 1:]
        else:
            tail = ''
        current_id = current_id[:idx] + x + tail
        idx -= 1
    if spillover > 0:
        current_id = 'A' + current_id
    return current_id

start_id = 'A'

