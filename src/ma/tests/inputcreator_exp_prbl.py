import ma.const

import random
import math
import os
import operator

job_id = 8
no_of_nodes = 6
task_capacity = 4
map_steps = 20
map_to_reduce_inps = 4
no_of_files = no_of_nodes * task_capacity * map_steps
file_size = 32 * 1024 * 1024
no_of_keys = int((no_of_files / map_to_reduce_inps) * 100)
dir = '/state/partition1/datasets/' + str(job_id)
#dir = 'C:/Users/Ahmad/Desktop/test/' + str(job_id)

words_inserted = 0

words_count_filename = dir + '/word_count.txt'


def get_cycled_word(i, no_letters):
    word = ''
    for j in range(no_letters):
        word += 'a'
    pos = 0
    while i > 0:
        letter = chr(ord('a') + (i % 26))
        i = int(i / 26)
        prv_word = ''
        if pos != 0:
            prv_word = word[-pos:]
        word = word[:-1-pos] + letter + prv_word
        pos += 1
        if pos >= no_letters:
            raise
    return word

keys = []
key_chances = []

def generate_chances():
    global key_chances
    # key_chances = ceil(exp(0:according-to-no-keys:12) ./ 100) * 10;
    key_chances = list(range(no_of_keys))
    key_chances = [(x / float(no_of_keys)) * 12 for x in key_chances]
    key_chances = [math.exp(x) / 100 for x in key_chances]
    key_chances = [math.ceil(x) * 10 for x in key_chances]


def generate_keys(words_count_dict):
    counter = 0
    for idx in range(no_of_keys):
        word = get_cycled_word(idx, 6)
        word = word + '.' + word + '.' + word
        keys.append((word, int(key_chances[counter])))
        words_count_dict[word] = 0
        counter += 1


def make_list():
    return_list = []
    for i in keys:
        for y in range(i[1]):
            return_list.append(i[0])
    return return_list


if __name__ == '__main__':
    words_count_dict = {}
    generate_chances()
    generate_keys(words_count_dict)
    #print words_count_dict
    print('No of keys', len(keys))
    list = make_list()
    print('Length choice list:', len(list))

    # ensure the destination directory exists
    if not os.path.exists(dir):
        os.makedirs(dir)
    
    for file_no in range(no_of_files):    
        filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_map_input_filename, job_id, file_no)
        filepath = dir + os.sep + filename
        
        print(filepath)
        fd = open(filepath, 'w+')
        random.seed()
        
        curr_file_size = 0
        words_inserted = 0
        separator = ' '
        while curr_file_size < file_size:
            word = random.choice(list)
            words_count_dict[word] = words_count_dict[word] + 1
            word = word + separator
            curr_file_size += len(word)
            words_inserted += 1
            if words_inserted % 30 == 0:
                random.seed()
            fd.write(word)
        
        print('File output', filename, 'Word count', words_inserted)
        
        fd.close()
    
    total_word_count = 0
    list_word_count = []
    for word in words_count_dict:
        list_word_count.append((word, words_count_dict[word]))
        total_word_count += words_count_dict[word]
    
    list_word_count.sort(key=operator.itemgetter(1), reverse=True);
    fd = open(words_count_filename, 'w+')
    for tpl in list_word_count:
        fd.write(str(tpl) + '\n')
    
    fd.close()
    
    print("Total no. of words inserted:", total_word_count)
    print("Length choice list:", len(list))
    print("The latter value should be less than the former for a fair chance of insertion to each word")
