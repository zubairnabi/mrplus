"""server"""

import socket, sys
import time
#dest = ('<broadcast>', 51423)
dest = ('192.168.216.255', 14418)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

print("Looking for replies; press Ctrl-C to stop.")
for i in range(1,30,1):
    s.sendto("Broadcast Packet no. " + str(i), dest)
    time.sleep(3.5)
