import subprocess
import pickle
from ma.commons.core.maptipinfo import MapTIPInfo

if __name__ == "__main__":
    
    proc = subprocess.Popen('mpstat', shell=True, stdout=subprocess.PIPE)
    print('MAIN - process spawned: ', proc.pid)
    print('MAIN - waiting')
    msg = proc.communicate()[0]
    
    proc.wait()
    print('MAIN - process finished off')