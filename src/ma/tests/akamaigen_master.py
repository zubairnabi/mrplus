import os
import ma.const
import ma.fs.dfs.dfsflags as dfsflags
import ma.utils.location

python_exec_path = ma.const.XmlData.get_filepath_str_data(ma.const.xml_python_exec) + ' -u'
akamai_gen_exec_path = os.path.join(ma.utils.location.get_src_ma_path(), 'tests', 'akamaigen.py')

no_structs = 20
maps_per_struct = 62
running_map_id = 0

job_id = 14             # also change in akamaigen.py
files_per_map = 1       # also change in akamaigen.py

mean = 20
increment_mean = 2
std_dev = 17
change_std_dev = 0

mfs = ""
xml_str = dfsflags.DfsFlags._DfsFlags__mapflag_template

for struct_id in range(no_structs):
    print("---> Struct %d - mean %d - std dev %d" % (struct_id, mean, std_dev))
    os.system(python_exec_path + ' ' + akamai_gen_exec_path + ' ' + str(mean) + ' ' + str(std_dev) + ' ' + str(running_map_id))
    
    
    for map_id in range(maps_per_struct):
        inputs = []
        
        for file_id in range(files_per_map):
            file_no = (map_id * files_per_map) + file_id
            out_filename = ma.const.JobsXmlData.get_str_data(ma.const.xml_map_input_filename, job_id, running_map_id + file_no)
            inputs.append((out_filename, 0, 0))
            
        mfs += dfsflags.DfsFlags.create_map_flags_stub(running_map_id + map_id, inputs, struct_id)
    
    mean += increment_mean
    std_dev += change_std_dev
    running_map_id += maps_per_struct * files_per_map
    
    
xml_str = xml_str % mfs

# write to file
mf_filename = ma.const.XmlData.get_str_data(ma.const.xml_map_flags_filename)
mf_fd = open(mf_filename, 'w+')
mf_fd.write(xml_str)
