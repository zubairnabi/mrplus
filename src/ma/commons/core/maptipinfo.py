from .taskinprogressinfo import *
from . import constants


class MapTIPInfo(TaskInProgressInfo):
    """This class derives from TaskInfo and keeps data that is relevant to a 
    map task only
    """
    
    def __init__(self, task_id, job_id, input_data = [], struct_data=constants.STRUCT_ID_DEFAULT):
        
        TaskInProgressInfo.__init__(self, task_id, job_id, MAP)
        
        '''the input array picked up from the map stub on the hdfs has the following structure:
        list has three items per entry
        1. input_data_path
        2.offset in the file to read data from
        3.the number of bytes to be read after the offset
        '''
        self.struct_data = struct_data
        
        self.input_data = input_data
        
        # times for stats
        self.start_time = 0.0
        self.end_time = 0.0
                
  