from .constants import *
from .action import *

class CompleteJobsAction(Action):

    """this is an Action kind of object sent to all NTs to inform them that  
    certain jobs are complete and NT can do housekeeping regarding this job's data;
    update the local task scheduler at all NTs
    """
    
    def __init__(self, jobs_list):
    
        Action.__init__(self, COMPLETEJOBSACTION_TYPE)
        
        self.jobs_list= jobs_list
    
    