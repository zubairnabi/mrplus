from .constants import *

class TaskInfo(object):
    
    """this class holds info for a specific task controlled by a NodeTracker. 
    The heartbeat sent by the Node to the HDFSInteracter is sent a list of such object
    """
    
    def __init__(self, type=TASK_INFO_TYPE, taskid=INVALID_TASK_ID, jobid=INVALID_JOB_ID, map_or_reduce=MAP):
        
        self.task_id = taskid
        self.job_id = jobid
        
        self.type = type
        
        #whether the task is a map(0) or a reduce(1) or a combiner(2)
        self.map_or_reduce = map_or_reduce        
        
        self.task_id_tag = str(self.job_id) + str(self.map_or_reduce) + str(self.task_id)
        
    
    def returnTaskID(self):
        """returns a taskinfo identifier"""
        
        return self.task_id_tag