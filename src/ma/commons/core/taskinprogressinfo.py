from .taskinfo import *
    
class TaskInProgressInfo(TaskInfo):
    
    """ This class will hold specific information for a task that is running; 
    Note: could be sent to the HDFSInteracter to update task progress                                                              
    """
    
    def __init__(self, taskid=INVALID_TASK_ID, jobid=INVALID_JOB_ID, mapflag=MAP):
        
        TaskInfo.__init__(self, TASK_INFO_TYPE, taskid, jobid, mapflag)
        
        #indicates to what percentage is the task has been completed
        self.percent_complete = 0
        
        #indicates the number of times this running task has failed 
        #(and now being re-run again)
        self.times_failed = 0
        


