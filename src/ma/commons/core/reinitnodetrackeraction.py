from .constants import *
from .action import *
    
class ReinitNodeTrackerAction(Action):
    
    """this class derives from the class Action and dictates that NodeTracker 
    it is sent to be reinitialized. It is usually cause by when a NodeTracker
    sends a heartbeat so late that the JobsTracker thinks the node is dead. 
    Every time node returns from the dead, it needs to re-initialize i.e. 
    throw away all the tasks it contains.
    """
    
    def __init__(self):
        
        Action.__init__(self, REINITNODETRACKERACTION_TYPE)