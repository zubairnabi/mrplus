from .constants import *
from .action import *
    
class KillTaskAction(Action):
    
    """This class derives from the class Action and dictates that a task be
     killed off at the NodeTracker.
    """
    
    def __init__(self, job_id, task_id, map_or_red = MAP):
        
        Action.__init__(self, KILLTASKACTION_TYPE)
       
        self.job_id = job_id
       
        self.task_id = job_id
        
        self.map_or_red = map_or_red