class NodeTrackerStatus(object):
    """this structure holds the TaskInfo objects list for a specific NodeTracker. this 
    list is sent to the JobsTracker along with a health ping to deliver the status of 
    tasks at this NodeTracker   
    """
    
    def __init__(self, nodetracker_id, nodetracker_ip=""):
        
        self.nodetracker_id = nodetracker_id
        self.nodetracker_ip = nodetracker_ip
        
        #list of task_info objects 
        self.tasks_info = []
        
        
    def reinitialize(self):
        """The TaskTrackerStaus object is reinitialized for every call to the 
        transmitHeartbeat method of the NodeTracker ... __tasks_info list is rebuilt
        """
        
        self.tasks_info = []
        
        
    def addTaskInfo(self, task_info):
        """this function adds a TaskInfo object to be sent to the JobsTracker 
        in a heartbeat
        """
        
        self.tasks_info.append(task_info)
        