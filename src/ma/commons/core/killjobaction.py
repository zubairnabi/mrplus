from .constants import *
from .action import *
    
class KillJobAction(Action):
    
    """this class derives from the class Action and dictates that all tasks of 
    a certain job will be killed off at the NodeTracker.
    """
    
    def __init__(self, job_id):
        
        Action.__init__(self, KILLJOBACTION_TYPE)
       
        self.job_id = job_id
    