"""This module is used to keep statistics about a file transfer. It is also 
used to keep identification data of each file transfer. This information
could be file transfers from anywhere, may it be the DFS or over the network 
"""

from . import constants
import time

__start = ord('A')
__limit = ord('Z') - ord('A') + 1

    
class FileTransferInfo(object):
    """This class stores information about each File Transfer, so that
    can be logged correctly
    """
    
    def __init__(self, transfer_id, file_info, file_size, job_id=None):
        """The constructer marks the start of the transfer
        """
        self.id = transfer_id
        
        # basic file info
        self.file_info = file_info
        self.file_size = file_size 
        self.job_id = job_id
        
        # times for stats
        self.start_time = time.time()
        self.end_time = 0.0
    
    
    def note_transfer_start(self):
        """This function marks the start of the file transfer
        """
        self.start_time = time.time()
    
    
    def note_transfer_end(self):
        """This function marks the end of the file transfer
        """
        self.end_time = time.time()
         
    
def get_next_id(current_id):
    """Considering each file transfer has a string ID, this function 
    fetches the next ID in line. It moves so: A -> B, Z -> AA,
    AZ -> BA
    """
    
    idx = -1
    length = len(current_id)
    spillover = 1
    while spillover > 0 and abs(idx) <= length:
        temp = ord(current_id[idx]) - __start
        x = chr(((temp + spillover) % __limit) + __start)
        spillover = (temp + spillover) / __limit
        if idx < -1:
            tail = current_id[idx + 1:]
        else:
            tail = ''
        current_id = current_id[:idx] + x + tail
        idx -= 1
    if spillover > 0:
        current_id = 'A' + current_id
    return current_id
    