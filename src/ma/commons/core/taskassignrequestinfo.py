from .constants import *
from .taskinfo import *
    
class TaskAssgnRequestInfo(TaskInfo):
    
    """This class is passed from the NodeTracker to the HDFSInteracter and will 
    tell the suggested job_id of the job from which that NodeTracker could be 
    assigned a task
    """
    
    def __init__(self, job_id, map_or_reduce):
        
        """this function assigns a suggested job_id of a job given by the NT's 
        local task scheduler and also suggests whether a map should be picked up
        or a reduce
        """
        
        TaskInfo.__init__(self, NEWTASK_INFO_TYPE, INVALID_TASK_ID, job_id, map_or_reduce)