from .constants import *
from .action import *
    
class NewJobAction(Action):

    """this is an Action kind of object sent to all NTs to inform them that a 
    new job has been added to the map reduce framework and NT can update its 
    job pool 
    """
    
    def __init__(self, job_id, map_red_ratio):
    
        Action.__init__(self, 7)#NEWJOBACTION_TYPE)
        
        self.job_id = job_id
        self.map_red_ratio = map_red_ratio
        
        #also possibly other job related data
        