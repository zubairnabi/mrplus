from .constants import *
    
class Action(object):
    
    """ this class is passed from the JobsTracker back to the NodeTracker in 
    response to its periodic ping to the former, a list of such objects dictates
    what actions the JobsTracker wants the NodeTracker to perform
    """
    
    def __init__(self, type = ACTION_TYPE):
        
        """this variable specifies which subclass of the Action class this object 
        identifies with. 
        """
        
        self.action_type = type
        
        
        
        
    