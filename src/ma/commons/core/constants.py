"""this module defines the constants used in other various modules of the project!!!
"""

# separator
TASK_ID_SEP = '_'

# phase for tasks map or reduce
READY = 0
COMPUTATIONAL = 1
SHUFFLE = 2
COMPLETE = 3

# the number of bytes sent over TCP for job_id and task_id
JOB_ID_BYTES = 8
TASK_ID_BYTES = 8

# for indication by the scheduler whether to pick up the map task or reduce task
MAP = 'M'
REDUCE = 'R'

# for class TaskInfo default values
INVALID_TASK_ID = 0
INVALID_JOB_ID = 0

# for completed Task percentage progress; 100%
COMPLETE_TASK = 1

# invalid or default values for a map task
INVALID_INPUT_FILE = ""
STARTING_BYTE = -1
BYTES_TO_READ = 0

# taskInfo object types
TASK_INFO_TYPE = 0
NEWTASK_INFO_TYPE = 3
COMPLETEDTASK_INFO_TYPE = 1
FAILEDTASK_INFO_TYPE = 2

# action objects type
ACTION_TYPE = None
NEWMAPTASKACTION_TYPE = 0
NEWREDUCETASKACTION_TYPE = 1
KILLTASKACTION_TYPE = 2
KILLJOBACTION_TYPE = 3
REINITNODETRACKERACTION_TYPE = 4
DEADNODETRACKERACTION_TYPE = 5
COMPLETEJOBSACTION_TYPE = 6
NEWJOBACTION_TYPE = 7
REFRESHJOBSACTION_TYPE =8

# Default ids
STRUCT_ID_DEFAULT = 'A'
FILE_TRANSFER_ID_DEFAULT = 'A'
DFS_TRANSFER_ID_DEFAULT = 'A'
