import sys

from .constants import *
from .taskinprogressinfo import *

class CompletedTaskInfo(TaskInProgressInfo):
    
    """this class holds information of a single completed task. It is passed by
    a NodeTracker to the HDFSInteracter. 
    Note: A failed task is not a completed task
    """
    
    def __init__(self, taskid, jobid, map_or_reduce=MAP, struct_id=STRUCT_ID_DEFAULT, reduce_level=-1):
        
        TaskInProgressInfo.__init__(self, taskid, jobid, map_or_reduce) 
        
        self.type = COMPLETEDTASK_INFO_TYPE
        
        self.struct_id = struct_id  
        self.reduce_id = reduce_level
        
        #percentage_complete is changed to 1 or 100% showing task to be complete
        self.percentage_complete = COMPLETE_TASK
        
        self.type = COMPLETEDTASK_INFO_TYPE
        
        