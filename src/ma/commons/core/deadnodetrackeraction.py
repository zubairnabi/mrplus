from .constants import *
from .action import *
    
class DeadNodeTrackerAction(Action):
    
    """this class derives from the class Action and dictates that a certain 
    node in the cluster just died. This will help the NodeTracker adjust its
    IPTranslationTable. It will also make the NodeTracker delete any map 
    output intermediate data that it had received from the dead node, if the 
    job is in map / shuffle phase.
    """
    
    def __init__(self, dead_node_id):
        
        Action.__init__(self, DEADNODETRACKERACTION_TYPE)
        
        #the following is the dead NT id for  
        #NTs so that they can update their IPTranslation table
        self.dead_node_id = dead_node_id
         
    