from .constants import *
from .action import *
    
class NewMapTaskAction(Action):
    
    """this class holds information of a single new map task to be assigned to a 
    NodeTracker. It is passed by a HDFSInteracter to NodeTracker on request as 
    suggested by the local task scheduler of the NT; in case the suggested job 
    is still running and all tasks for this job are not already picked up 
    """
    
    def __init__(self, task_id, job_id, input_data, struct_id=STRUCT_ID_DEFAULT):
    
        Action.__init__(self, NEWMAPTASKACTION_TYPE)
       
        self.job_id = job_id
       
        self.task_id = task_id
       
        self.struct_id = struct_id
       
        #this is a double list where every entry is made of [filename, bytes_to_read, byte_offset] 
        self.input_data = input_data       

    