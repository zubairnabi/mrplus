from .constants import *
import ma.log


class TaskRunner(object):
    
    """ This is the class that initiates a user supplied map or reduce task as
    a forked child process so as to keep its code separate from the map reduce
    framework code.  
    """
    
    def __init__(self, tip_info, user_module_name, user_class_name):
        
        """This function is called to initiate a TaskRunner 
        """
        self.__log = ma.log.get_logger("ma.codes")
        self.threads = []
        self.tip_info = tip_info
                
        #make a thread which will write into the output file
            
        #TODO: get user map module name from job xml and import the module
        self.user_module_name = user_module_name
        self.user_class_name = user_class_name
        #self.user_class = eval(user_class_name)
        #self.user_module = __import__(self.user_module_name)
        #from ma.commons.core.map import
        
        self.__log.info('TaskRunner inited %s', tip_info.returnTaskID())
        
        try:
            self.user_class = __import__(self.user_module_name, globals(), locals(), [self.user_class_name], -1)
        except Exception as err_msg:
            self.__log.error('User module import error: %s', err_msg) 
    
    
    def startTask(self):
        """This func is called to start the user defined code as a separate 
        process
        """   
        
        raise NotImplementedError('The Task Runner is implemented by MapTaskRunner and ReduceTaskRunner')

