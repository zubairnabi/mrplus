import sys
import copy
from .constants import *
from .taskinprogressinfo import *
import ma.const


class TaskInProgress(object):
    """This class keeps the status of the task as a TaskInfo object, a TaskRunner
     and also provides certain functionality like launching the task etc
    """
    
    def __init__(self, nodetracker, tip_info = None):
        
        #in case no TIPInfo object sent in arguments make a TIPInfo with default
        #settings
        if tip_info is None:
            #this is the object which keeps the taskInprogress-info type of object
            self.tip_info = TaskInProgressInfo()
        else:
            #else make a deep copy of TIPInfo object sent as args
            self.tip_info = copy.deepcopy(tip_info ) 
              
        #this is the task runner that will initiate the task as a separate process        
        self.task_runner = None
        
        #this is the phase the task is in READY phase
        self.phase = READY
        
        #the node tracker ref of the NT it is running on
        self.nodetracker = nodetracker
        
        #this is the interval between this tasks health ping to the NodeTracker
        #READ THIS FROM A CONF FILE 
        self.task_ping_interval = ma.const.XmlData.get_float_data(ma.const.xml_task_ping_interval)    #task_ping_interval

        
    def returnChildID(self, child_pid):
        """this function is called from within the TaskRunner to return the pid
        of the child process initiated. To be used for health monitoring 
        """
        
        self.childprocess_pid = child_pid
        
        
    def launchTask(self):
        """this func calls on the TaskRunner to start a new task as a separate 
        child process
        """
        
        #this will start a timer controlled periodic health ping of the task to 
        #the nodetracker
        
        #arglist = [self.tip_info.job_id, self.tip_info.task_id]
        #self.pingtimer_id = self.nodetracker.timer.addTimer(self.task_ping_interval, self.nodetracker.healthPingFromTask,arglist)
        
        
    def localizeTask(self):
        """this func copies the input for the task and any configuration info (LIKE THE HEALTH PING INTERVAL)
        from the DFS to the local disc
        """
        
        pass
    
    
    def pingTheNodeTracker(self, progress = 0):
        """this func regularly pings the NodeTracker for registering health and
        delivering progress
        """
        
        pass
    
    
    def getMapOrReduce(self):
        """this function returns whether a task is map or reduce
        """
        
        return self.tip_info.map_or_reduce
    
        
    def killTask(self):
        """1) this function is meant to kill off a task running as a process on orders of the NT it 
        is running on! 
        2) it will also delete the timer that prompts a health ping to the NT
        3) must also clear the files the task was working on for house keeping 
        """
        
        # TODO: CODE for killing a spawned process for user given function of map or reduce
        
        self.nodetracker.timer.removeTimer(self.__pingtimer_id)
        
        # TODO: CODE for house keeping
        