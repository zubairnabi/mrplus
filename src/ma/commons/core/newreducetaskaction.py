from .constants import *
from .action import *
    
class NewReduceTaskAction(Action):
    
    """this class holds reduce task info like the reduce level this task 
    belongs to
    """
    
    def __init__(self, job_id, task_id, input_data, reduce_level=-1, struct_id=STRUCT_ID_DEFAULT):
        
        Action.__init__(self, NEWREDUCETASKACTION_TYPE)
        
        self.job_id = job_id
        self.task_id = task_id
        self.input_data = input_data
        
        self.struct_id = struct_id
                
        self.reduce_level = reduce_level
        
        