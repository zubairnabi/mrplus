from .taskinprogressinfo import *
from . import constants


class ReduceTIPInfo(TaskInProgressInfo):    
    """This class derives from TaskInfo and keeps data that is relevant to a 
    reduce task only
    """
    
    def __init__(self, job_id , task_id , input_data, reduce_level=0, struct_data=constants.STRUCT_ID_DEFAULT):
        
        TaskInProgressInfo.__init__(self, task_id, job_id, REDUCE)
        
        #input data is a dict key -> list_of_values  
        self.input_data = input_data 
        
        #by default the reduce level of any reduce task is 0 or the first level
        self.reduce_level = reduce_level
        
        self.struct_data = struct_data
        
        # times for stats
        self.start_time = 0.0
        self.end_time = 0.0
        
        self.list_of_all_keys = None


