from .constants import *
from .taskinprogressinfo import *

class FailedTaskInfo(TaskInProgressInfo):
    
    """this class holds information of a single failed task. It is passed by
    a NodeTracker to the specific HDFSInteracter. 
    Note: A failed task is not a completed task
    """
    
    def __init__(self, taskid, jobid, map_or_reduce = MAP):
        
        TaskInProgressInfo.__init__(taskid, jobid, map_or_reduce)
        self.type = FAILEDTASKINFO_TYPE
        
        #increment the times failed by one
        TaskInProgressInfo.times_failed += 1
        
        self.type = FAILEDTASK_INFO_TYPE