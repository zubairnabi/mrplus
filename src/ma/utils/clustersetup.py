
# ips present in the cluster, used only for transfer of code, transfer of 
#    stats, and remote running (EXCLUDING THE MASTER)
#CLUSTER_IPS = ['192.168.10.249', '192.168.10.250', '192.168.10.251', '192.168.10.253', '192.168.10.254']
#CLUSTER_IPS = ['192.168.100.248', '192.168.100.249', '192.168.100.250', '192.168.100.251', '192.168.100.252', '192.168.100.253']
CLUSTER_IPS = ['192.168.100.243', '192.168.100.244', '192.168.100.245', '192.168.100.248', '192.168.100.250', '192.168.100.251', '192.168.100.252', '192.168.100.253', '192.168.100.254']
#FRONT_END_IP = '203.128.0.145'
FRONT_END_IP = '203.128.4.45'
MASTER_INTERNAL_IP = '192.168.100.1'

FRONT_END_USER = 'root'
#FRONT_END_PWD = 'june2008'
FRONT_END_PWD = 'mr+nsdi09'
BACK_END_ACCESS_USER = 'root'

REMOTE_RUN_PATH = '~/'
MASTER_RUN_PATH = '/state/partition1/datasets/%s/'

STATS_DEST_PATH = '/state/partition1/stats-run1/'
COPY_STATS_FILES = ['ma_stats%s.csv', 'estimator_stats%s.csv', 'threshold_stats%s.csv', 'ma_logger%s.log', 'nohup%s.out']
COPY_STATS_FILES_MASTER = ['ma_stats.csv', 'estimator_stats.csv', 'threshold_stats.csv', 'ma_logger.log', 'ma_master.log', 'reduce_final.out', 'jobconf.xml', 'mapflags.xml', 'job_complete.out', 'nohup.out', 'nohup_master.out']