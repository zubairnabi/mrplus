from threading import Thread


class AThread(Thread):
    """this class is used by another class when it wants to asynchronously 
    call a function. the thread starts as the func begins and dies when the
    func ends
    """
    
    def __init__(self, callback_func, arglist):
        """this func assigns the func to be called and also starts the thread
        """
        
        self.__callback_func = callback_func
        self.__arglist = arglist
        
        Thread.__init__(self)
        
              
    def run(self):
        """this func initiates the thread and the func the thread is to run
        """
        
        #apply(self.__callback_func, self.__arglist)
        
        if(len(self.__arglist) == 0):
            self.__callback_func.__call__()
        elif(len(self.__arglist) == 1):
            self.__callback_func.__call__(self.__arglist[0])
        elif(len(self.__arglist) == 2):
            self.__callback_func.__call__(self.__arglist[0],self.__arglist[1])
        elif(len(self.__arglist) == 3):
            self.__callback_func.__call__(self.__arglist[0], self.__arglist[1], self.__arglist[2])
        elif(len(self.__arglist) == 4):
            self.__callback_func.__call__(self.__arglist[0],self.__arglist[1], self.__arglist[2],self.__arglist[3])
        