import ma.const as const

import os
import os.path
import sys



def get_proj_basekeeper_path():
    """Gets the path which holds the project's base path
    """
    
    path = str(const) # PATH extracted..
    idx = path.find('from ', 0)
    idx += 6
    path = path[idx:-3-len(os.path.join('MRImprov', 'src', 'ma', 'const.py'))]
    return path


def get_proj_base_path():
    """Gets the project's base path
    """
    
    path = get_proj_basekeeper_path()
    path = os.path.join(path, 'MRImprov')  
    return path


def get_src_ma_path():
    """Gets the path where all the src code for the project is
    """
    
    path = get_proj_base_path()
    path = os.path.join(path, 'src', 'ma')
    return path


def get_ma_conf_path():
    """Get the path where all the configuration files are
    """
    
    path = get_src_ma_path()
    path = os.path.join(path, 'conf')
    return path


def get_mr_path():
    """Gets the base path where all the MapReduce code is
    """
    
    path = get_src_ma_path()
    path = os.path.join(path, 'mapred')
    return path


def get_mrplus_master_path():
    """Gets the base path where all the MR+ Master specific code is
    """
    
    path = get_src_ma_path()
    path = os.path.join(path, 'mrplus_master')
    return path


def get_mrimprov_path():
    """Gets the base path where all the MR+ (MRImprov) code is
    """
    
    path = get_src_ma_path()
    path = os.path.join(path, 'mrimprov')
    return path


def get_scripts_path():
    """Gets the path to the bin directory where all the scripts are
    """
    
    path = get_proj_base_path()
    path = os.path.join(path, 'scripts')
    return path


if __name__ == "__main__":
    print(get_proj_base_path())
    print(get_src_ma_path())
    print(get_ma_conf_path())
    print(get_mr_path())
    print(get_mrplus_master_path())
    print(get_mrimprov_path())
    print(get_scripts_path())
    